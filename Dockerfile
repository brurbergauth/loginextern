FROM node:latest AS builder
WORKDIR /app
COPY . ./
RUN npm install
RUN npm run build

FROM nginx:alpine
RUN mkdir /etc/nginx/logs
RUN mkdir /usr/share/nginx/html/extern-login
COPY --from=builder /app/public /usr/share/nginx/html/extern-login
COPY nginx.conf /etc/nginx/nginx.conf
COPY default.conf /etc/nginx/conf.d/default.conf
EXPOSE 80 443
CMD ["nginx", "-g", "daemon off;"]
